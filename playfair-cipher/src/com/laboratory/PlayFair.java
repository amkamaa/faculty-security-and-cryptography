package com.laboratory;

public class PlayFair {
    private char[][] matrix = new char[5][5];
    private String alphabet = "abcdefghijklmnopqrstuvwxyz".toUpperCase();

    public PlayFair() {
    }

    public void encrypt(final String password, final String message) {
        buildMatrix(removeDuplicates(password));
        showMatrix();

        String result = "";
        String[] digrams = getDigrams(message.toUpperCase().replace("J", "I"));

        for (String digram : digrams) {
            Pair ch1At = getIndex(digram.charAt(0));
            Pair ch2At = getIndex(digram.charAt(1));

            char ch1, ch2;
            if (ch1At.column == ch2At.column) {
                ch1 = matrix[(ch1At.row + 1) % 5][ch1At.column];
                ch2 = matrix[(ch2At.row + 1) % 5][ch2At.column];
            } else if (ch1At.row == ch2At.row) {
                ch1 = matrix[ch1At.row][(ch1At.column + 1) % 5];
                ch2 = matrix[ch2At.row][(ch2At.column + 1) % 5];
            } else {
                ch1 = matrix[ch1At.row][ch2At.column];
                ch2 = matrix[ch2At.row][ch1At.column];
            }
            result += ch1 + "" + ch2;
        }

        System.out.println("Encrypted message: " + result);

    }

    public void decrypt(final String password, final String message) {
        buildMatrix(removeDuplicates(password));
        showMatrix();

        String result = "";
        String[] digrams = getDigrams(message.toUpperCase().replace("J", "I"));

        for (String digram : digrams) {
            Pair ch1At = getIndex(digram.charAt(0));
            Pair ch2At = getIndex(digram.charAt(1));

            char ch1, ch2;
            if (ch1At.column == ch2At.column) {
                ch1 = matrix[ch1At.row <= 0 ? 4 : ch1At.row - 1][ch1At.column];
                ch2 = matrix[ch2At.row <= 0 ? 4 : ch2At.row - 1][ch2At.column];
            } else if (ch1At.row == ch2At.row) {
                ch1 = matrix[ch1At.row][ch1At.column <= 0 ? 4 : ch1At.column - 1];
                ch2 = matrix[ch2At.row][ch2At.column <= 0 ? 4 : ch2At.column - 1];
            } else {
                ch1 = matrix[ch1At.row][ch2At.column];
                ch2 = matrix[ch2At.row][ch1At.column];
            }
            result += ch1 + "" + ch2;
        }

        System.out.println("Decrypted message: " + result);

    }

    private Pair getIndex(final char ch) {
        boolean found = false;
        int i = 0, j = 0;

        while (!found) {
            if (matrix[i][j] == ch) {
                found = true;
            } else {
                j++;
                if (j == matrix.length) {
                    i++;
                    j = 0;
                }
            }
        }

        return new Pair(i, j);
    }

    private String[] getDigrams(final String input) {

        String message = formatMessage(input);
        String digrams[] = new String[message.length() / 2];
        int j = 0;

        for (int i = 0; i < message.length(); i = i + 2) {
            digrams[j] = message.substring(i, i + 2);
            j++;
        }

        return digrams;
    }

    private String formatMessage(final String input) {
        String message = input.replace(" ", "");
        StringBuilder result = new StringBuilder(message);

        for (int i = 0; i < message.length() - 1; i += 2) {
            if (result.charAt(i) == result.charAt(i + 1)) {
                result.insert(i + 1, "X");
            }
        }

        if (result.length() % 2 == 1) {
            result.append("X");
        }

        return result.toString();
    }

    private void buildMatrix(final String password) {
        final String upperCasePassword = password.toUpperCase();
        int i = 0, j = 0;

        for (int k = 0; k < password.length(); k++) {
            matrix[i][j++] = upperCasePassword.charAt(k);

            if (j == matrix.length) {
                i++;
                j = 0;
            }
        }

        for (int k = 0; k < alphabet.length(); k++) {
            if (!upperCasePassword.contains(String.valueOf(alphabet.charAt(k))) && alphabet.charAt(k) != 'J') {
                matrix[i][j++] = alphabet.charAt(k);
            }

            if (j == matrix.length) {
                i++;
                j = 0;
            }
        }
    }

    private String removeDuplicates(final String message) {
        String result = "";
        for (int i = 0; i < message.length(); i++) {
            if (!result.contains(String.valueOf(message.charAt(i)))) {
                result += String.valueOf(message.charAt(i));
            }
        }

        return result;
    }

    private void showMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
