package com.laboratory;

public class Pair {
    public int row, column;

    public Pair(int row, int column) {
        this.row = row;
        this.column = column;
    }
}
