/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algotithms.merklehellman;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author wlaszlo
 */
public class MerkleHellman {
    Random rand = new Random();

    public MerkleHellman() {
    }
    
    public String encrypt(boolean[] message, ArrayList<Integer> w, int p) {
        int r;
        
        do {
            r = rand.nextInt(1000000) + 2;
        } while(getGreatCommonDivisor(r, p) == 1);
        
        int[] pubKey = calculatePublicKey(w, p, r);
        String result = "r=" + r + 
                "\nPublic Key: (" + toString(pubKey) + ")\n";
        
        for (int i=0; i<w.size(); i++) {
            result += pubKey[i]*toInt(message[i]);
        }
        
        return result;
    }
    
    public String decrypt(int message, ArrayList<Integer> w, int p, int r) {
        return "";
    }
    
    private long getGreatCommonDivisor(long a, long b) {
        while (a != b) {
            if (a < b) {
                b -= a;
            } else {
                a -= b;
            }
        }

        return a;
    }
    
    private int[] calculatePublicKey(ArrayList<Integer> w, int p, int r) {
        int[] result = new int[w.size()];
        
        for(int i=0; i<w.size(); i++) {
            result[i] = (r*w.get(i)) % p;
        }
        
        return result;
    }
    
    private int[] calculatePrivateKey(ArrayList<Integer> w, int p, int r) {
        int[] result = new int[w.size()];
        
        for(int i=0; i<w.size(); i++) {
            result[i] = (r*w.get(i)) % p;
        }
        
        return result;
    }
    
    private int toInt(boolean value) {
        if (value) {
            return 1;
        } else {
            return 0;
        }
    }
    
    private String toString(int[] array) {
        String result = "";
        
        for (int i=0; i<array.length; i++) {
            result += array[i] + ", ";
        }
        
        return result.substring(0, result.length()-2);
    }
}
