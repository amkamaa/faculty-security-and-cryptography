/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.playfair;

/**
 *
 * @author wlaszlo
 */
public class Pair {
    public int row, column;

    public Pair(int row, int column) {
        this.row = row;
        this.column = column;
    }
}
