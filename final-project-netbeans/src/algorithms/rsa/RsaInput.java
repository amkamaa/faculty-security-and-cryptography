/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.rsa;

/**
 *
 * @author wlaszlo
 */
public class RsaInput {
  public long p, q, message;

  @Override
  public String toString() {
    return "p=" + p +
        ", q=" + q +
        ", message=" + message;
  }

  public RsaInput(long p, long q, long message) {
    this.p = p;
    this.q = q;
    this.message = message;
  }
}
