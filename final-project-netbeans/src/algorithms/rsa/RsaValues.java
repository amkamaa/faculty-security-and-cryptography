/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.rsa;

/**
 *
 * @author wlaszlo
 */
public class RsaValues {
  long n, phi, e, d, finalMessage;

  RsaValues(long n, long phi, long e, long d, long finalMessage) {
    this.n = n;
    this.phi = phi;
    this.e = e;
    this.d = d;
    this.finalMessage = finalMessage;
  }

  @Override
  public String toString() {
    return "n=" + n +
        ", phi=" + phi +
        ", e=" + e +
        ", d=" + d;
  }
}
