/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.caesar;

/**
 *
 * @author wlaszlo
 */
public class CaesarCipher {
    private String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public CaesarCipher() {
    }
    
    public CaesarCipher(String alphabet) {
        this.alphabet = alphabet;   
    }

    public void setAlphabet(String alphabet) {
        this.alphabet = alphabet;
    }
    
    public String encrypt(String message, int k) {
        message = message.toUpperCase();
        String result = "";

        for (int i = 0; i < message.length(); i++) {
            char deltaPos = alphabet.charAt((alphabet.indexOf(message.charAt(i)) + k) % 26);
            result += (message.charAt(i) == ' ' ? " " : deltaPos);
        }

        return result;
    }

    public String decrypt(String message, int k) {
        message = message.toUpperCase();
        String result = "";

        for (int i = 0; i < message.length(); i++) {
            int deltaPos = alphabet.indexOf(message.charAt(i)) - k;
            result += (message.charAt(i) == ' ' ? " " : alphabet.charAt((deltaPos < 0 ? alphabet.length() + deltaPos : deltaPos)));
        }
        
        return result;
    }
}
