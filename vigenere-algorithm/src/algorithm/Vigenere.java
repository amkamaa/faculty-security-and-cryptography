package algorithm;

public class Vigenere {
  private static String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  void encrypt(final String inputMessage, final String inputKey) {
    String message = inputMessage.toUpperCase();
    String key = inputKey.toUpperCase();
    StringBuilder result = new StringBuilder();
    int keyIndex = -1;

    for (int i = 0; i < message.length(); i++) {
      if (message.charAt(i) != ' ') {
        int keyCharIdx = alphabet.indexOf(key.charAt(++keyIndex % key.length()));
        int alphCharIdx = alphabet.indexOf(message.charAt(i));
        char ch = alphabet.charAt((alphCharIdx + keyCharIdx) % alphabet.length());
        result.append(ch);
      } else {
        result.append(' ');
      }
    }

    System.out.println("Encryption" + " with key " + key);
    System.out.println("Decrypted message: " + message);
    System.out.println("Encrypted message: " + result + "\n");
  }

  void decrypt(final String inputMessage, final String inputKey) {
    String message = inputMessage.toUpperCase();
    String key = inputKey.toUpperCase();
    StringBuilder result = new StringBuilder();
    int keyIndex = -1;

    for (int i = 0; i < message.length(); i++) {
      if (message.charAt(i) != ' ') {
        int keyCharIdx = alphabet.indexOf(key.charAt(++keyIndex % key.length()));
        int alphCharIdx = alphabet.indexOf(message.charAt(i));
        int diff = alphCharIdx - keyCharIdx;
        int newIdx = (diff < 0 ? (alphabet.length() + diff) : diff);
        char ch = alphabet.charAt((newIdx) % alphabet.length());
        result.append(ch);
      } else {
        result.append(' ');
      }
    }

    System.out.println("Decryption" + " with key " + key);
    System.out.println("Encrypted message: " + message);
    System.out.println("Decrypted message: " + result + "\n");
  }
}
