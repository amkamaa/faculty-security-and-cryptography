package algorithm;


public class Main {
  public static void main(String[] args) {
    Vigenere vigenere = new Vigenere();
    vigenere.encrypt("THIS IS AN EXAMPLE OF THE VIGENERE CIPHER", "vector");
    vigenere.decrypt("OLKL WJ VR GQODKPG HT KCI XBUVIITX QZKLGK", "vector");
  }
}
