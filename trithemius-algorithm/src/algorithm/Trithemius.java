package algorithm;

public class Trithemius {
  private char[][] matrice = new char[26][26];

  Trithemius() {
  }

  private void generateMatrix() {
    for (int i = 0; i < 26; i++) {
      matrice[0][i] = (char) ('A' + i);
      matrice[i][0] = (char) ('A' + i);
    }
    for (int i = 1; i < 26; i++)
      for (int j = 1; j < 26; j++) {
        matrice[i][j] = (char) ((matrice[i][j - 1] + 1 - 'A') % 26 + 'A');
      }
  }

  private void printMatrix() {
    for (int i = 0; i < 26; i++) {
      for (int j = 0; j < 26; j++)
        System.out.print(matrice[i][j] + " ");
      System.out.println();
    }
  }

  private char searchMatrix(char t, char c) {
    int linie = 0;
    int coloana = 0;
    for (int i = 0; i < 26; i++) {
      if (matrice[i][0] == t)
        linie = i;
      if (matrice[0][i] == c)
        coloana = i;
    }
    return matrice[linie][coloana];

  }

  private char decodeMatrix(char t, char c) {
    int col = c - 'A';
    for (int i = 1; i < 26; i++)
      if (matrice[i][col] == t)
        return matrice[i][0];
    return 'A';
  }

  void encrypt(final String inputMessage, final String inputKey) {
    String message = inputMessage.toUpperCase();
    String key = inputKey.toUpperCase();
    char[] encrypted = new char[message.length()];

    generateMatrix();

    int k = 0;
    for (int i = 0; i < message.length(); i++) {
      encrypted[i] = message.charAt(i);
      if (message.charAt(i) != ' ') {
        if (k == key.length()) {
          k = 0;
        }
        encrypted[i] = searchMatrix(message.charAt(i), key.charAt(k));
        k++;
      }
    }

    System.out.println("Encryption" + " with key " + key);
    System.out.println("Decrypted message: " + message);
    System.out.print("Encrypted message: ");
    System.out.println(encrypted);
  }

  void decrypt(final String inputMessage, final String inputKey) {
    String message = inputMessage.toUpperCase();
    String key = inputKey.toUpperCase();
    char[] decrypted = new char[message.length()];
    StringBuilder result = new StringBuilder();

    int k = 0;
    for (char ch : decrypted) {
      if (ch != ' ') {
        if (k == key.length()) {
          k = 0;
        }
        result.append(decodeMatrix(ch, key.charAt(k)));
        k++;
      } else
        result.append(' ');
    }

    System.out.println("Decryption" + " with key " + key);
    System.out.println("Encrypted message: " + message);
    System.out.println("Decrypted message: " + result);
  }
}
