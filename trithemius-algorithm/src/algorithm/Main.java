package algorithm;

public class Main {

  public static void main(String[] args) {
	  Trithemius trithemius = new Trithemius();

	  trithemius.encrypt("dCode Trithemius", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    System.out.println();
	  trithemius.decrypt("DDQGI YXPBQOXUHG", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  }
}
