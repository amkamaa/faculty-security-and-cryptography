package frames;

import algorithms.playfair.Playfair;

import javax.swing.*;

class PlayfairPanel extends JPanel {
  private JButton jButtonSubmit;
  private JComboBox<String> jComboBoxType;
  private JLabel jLabel1;
  private JLabel jLabelAlphabet;
  private JLabel jLabelInput;
  private JLabel jLabelOutput;
  private JLabel jLabelPassword;
  private JScrollPane jScrollPane1;
  private JScrollPane jScrollPane2;
  private JScrollPane jScrollPane4;
  private JTextArea jTextAreaInput;
  private JTextArea jTextAreaMatrix;
  private JTextArea jTextAreaOutput;
  private JTextField jTextFieldAlphabet;
  private JTextField jTextFieldPassword;
  private Playfair playfair;


  PlayfairPanel() {
    playfair = new Playfair();

    initComponents();
    updateLabelMessageForEncryption();
    jTextAreaOutput.setEditable(false);
  }

  private void initComponents() {

    jScrollPane1 = new JScrollPane();
    jTextAreaInput = new JTextArea();
    jComboBoxType = new JComboBox<>();
    jLabelInput = new JLabel();
    jLabelOutput = new JLabel();
    jScrollPane2 = new JScrollPane();
    jTextAreaOutput = new JTextArea();
    jButtonSubmit = new JButton();
    jTextFieldAlphabet = new JTextField();
    jLabelAlphabet = new JLabel();
    jLabelPassword = new JLabel();
    jTextFieldPassword = new JTextField();
    jScrollPane4 = new JScrollPane();
    jTextAreaMatrix = new JTextArea();
    jLabel1 = new JLabel();

    setPreferredSize(new java.awt.Dimension(600, 400));

    jTextAreaInput.setColumns(20);
    jTextAreaInput.setRows(5);
    jScrollPane1.setViewportView(jTextAreaInput);

    jComboBoxType.setModel(new DefaultComboBoxModel<>(new String[]{"Encrypt message", "Decrypt message"}));
    jComboBoxType.addActionListener(this::jComboBoxTypeActionPerformed);

    jLabelInput.setText("jLabel1");

    jLabelOutput.setText("jLabel1");

    jTextAreaOutput.setColumns(20);
    jTextAreaOutput.setRows(5);
    jScrollPane2.setViewportView(jTextAreaOutput);

    jButtonSubmit.setText("jButtonSubmit");
    jButtonSubmit.addActionListener(this::jButtonSubmitActionPerformed);

    jTextFieldAlphabet.setEditable(false);
    jTextFieldAlphabet.setText("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

    jLabelAlphabet.setText("Alphabet:");

    jLabelPassword.setText("Password:");

    jTextAreaMatrix.setEditable(false);
    jTextAreaMatrix.setColumns(5);
    jTextAreaMatrix.setRows(5);
    jTextAreaMatrix.setPreferredSize(null);
    jScrollPane4.setViewportView(jTextAreaMatrix);

    jLabel1.setText("Matrix:");

    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBoxType, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                        .addGap(101, 101, 101)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelPassword)
                            .addComponent(jLabelAlphabet))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldAlphabet, GroupLayout.PREFERRED_SIZE, 273, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldPassword, GroupLayout.PREFERRED_SIZE, 273, GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelOutput, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonSubmit, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelInput, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                                .addGap(294, 294, 294)))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(jScrollPane4))))
                .addContainerGap())
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxType, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldAlphabet, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelAlphabet))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPassword))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabelInput))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelOutput)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonSubmit)
                .addContainerGap())
    );

    jLabelPassword.getAccessibleContext().setAccessibleName("Password::");
  }

  private void jComboBoxTypeActionPerformed(java.awt.event.ActionEvent evt) {
    System.out.println(jComboBoxType.getSelectedIndex());
    if (jComboBoxType.getSelectedIndex() == 0) {
      updateLabelMessageForEncryption();
    } else {
      updateLabelMessageForDecryption();
    }
  }

  private void jButtonSubmitActionPerformed(java.awt.event.ActionEvent evt) {
    playfair.setAlphabet(jTextFieldAlphabet.getText());

    if (jComboBoxType.getSelectedIndex() == 0) {
      jTextAreaOutput.setText(playfair.encrypt(jTextFieldPassword.getText(), jTextAreaInput.getText()));
    } else {
      jTextAreaOutput.setText(playfair.decrypt(jTextFieldPassword.getText(), jTextAreaInput.getText()));
    }

    jTextAreaMatrix.setText(playfair.getMatrix());
  }

  private void updateLabelMessageForDecryption() {
    jLabelInput.setText("Encrypted message:");
    jLabelOutput.setText("Plain message:");
    jButtonSubmit.setText("Decrypt message");
  }

  private void updateLabelMessageForEncryption() {
    jLabelInput.setText("Plain message:");
    jLabelOutput.setText("Encrypted message:");
    jButtonSubmit.setText("Encrypt message");

  }
}
