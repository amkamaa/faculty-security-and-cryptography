package frames;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;

public class MainFrame extends JFrame {
  private JPanel jPanelCaesar;
  private JPanel jPanelMerkleHellman;
  private JPanel jPanelPlayfair;
  private JPanel jPanelRsa;
  private JTabbedPane jTabbedPane1;

  private MainFrame() {
    initComponents();
  }

  private void initComponents() {
    jTabbedPane1 = new JTabbedPane();
    jPanelCaesar = new CaesarPanel();
    jPanelPlayfair = new frames.PlayfairPanel();
    jPanelRsa = new RsaPanel();
    jPanelMerkleHellman = new MerkleHellmanPanel();

    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setAlwaysOnTop(true);
    setMaximumSize(new Dimension(1400, 800));
    setMinimumSize(new Dimension(400, 200));

    jTabbedPane1.setMaximumSize(new Dimension(0, 0));
    jTabbedPane1.addChangeListener(this::jTabbedPane1StateChanged);

    jTabbedPane1.addTab("Caesar Cipher", jPanelCaesar);
    jTabbedPane1.addTab("Playfair", jPanelPlayfair);
    jTabbedPane1.addTab("RSA", jPanelRsa);
    jTabbedPane1.addTab("Merkle Hellman", jPanelMerkleHellman);

    GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
    );

    jTabbedPane1.getAccessibleContext().setAccessibleName("");

    getAccessibleContext().setAccessibleDescription("");

    pack();
  }

  private void jTabbedPane1StateChanged(ChangeEvent evt) {
  }

  public static void main(String args[]) {
    try {
      for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }

    EventQueue.invokeLater(() -> new MainFrame().setVisible(true));
  }
}
