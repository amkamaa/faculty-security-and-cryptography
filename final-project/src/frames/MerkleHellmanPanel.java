package frames;

import algorithms.merklehellman.MerkleHellman;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;

public class MerkleHellmanPanel extends JPanel {
  private JButton jButtonSubmit;
  private JLabel jLabel1;
  private JLabel jLabel2;
  private JLabel jLabel3;
  private JScrollPane jScrollPane1;
  private JTextField messageTextField;
  private JTextArea outTextArea;
  private JLabel outputLabel;
  private JLabel qLabel;
  private JSpinner qSpinner;
  private JComboBox<String> selectionComboBox;
  private JLabel wLabel;
  private JTextField wTextField;
  private MerkleHellman mkHellman;

  MerkleHellmanPanel() {
    mkHellman = new MerkleHellman();

    initComponents();
    updateTextForEncryption();
  }

  private void initComponents() {
    jScrollPane1 = new JScrollPane();
    outTextArea = new JTextArea();
    outputLabel = new JLabel();
    jButtonSubmit = new JButton();
    wLabel = new JLabel();
    selectionComboBox = new JComboBox<>();
    wTextField = new JTextField();
    jLabel2 = new JLabel();
    jLabel3 = new JLabel();
    qLabel = new JLabel();
    qSpinner = new JSpinner();
    jLabel1 = new JLabel();
    messageTextField = new JTextField();

    setPreferredSize(new java.awt.Dimension(600, 400));

    outTextArea.setColumns(20);
    outTextArea.setRows(5);
    jScrollPane1.setViewportView(outTextArea);

    outputLabel.setText("Output:");
    outTextArea.setEditable(false);

    jButtonSubmit.setText("Calculate");
    jButtonSubmit.addActionListener(evt -> jButtonSubmitActionPerformed(evt));

    wLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    wLabel.setText("w:");

    selectionComboBox.setModel(new DefaultComboBoxModel<>(new String[]{"Encryption", "Decryption"}));
    selectionComboBox.addActionListener(evt -> selectionComboBoxActionPerformed(evt));

    jLabel2.setText("Secvența supercrescătoare");

    jLabel3.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
    jLabel3.setText("(separați numerele prin spațiu)");

    qLabel.setText("q:");

    qSpinner.setModel(new SpinnerNumberModel(2, 2, null, 1));

    jLabel1.setText("Message:");

    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(outputLabel, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 428, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonSubmit, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addGap(0, 197, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(qLabel)
                            .addComponent(wLabel))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(wTextField)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(qSpinner, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(selectionComboBox, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(messageTextField, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(selectionComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(wLabel)
                    .addComponent(wTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(qLabel)
                    .addComponent(qSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(messageTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(outputLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonSubmit)
                .addContainerGap())
    );
  }

  private void jButtonSubmitActionPerformed(ActionEvent evt) {
    ArrayList<Integer> w = parseList(wTextField.getText());
    int q = (int) qSpinner.getValue();

    String result = "";

    try {
      String message = messageTextField.getText();
      boolean[] bits = getBits(message);

      if (selectionComboBox.getSelectedIndex() == 0) {
        validateInput(w, q);
        result = mkHellman.encrypt(bits, w, q);
      } else {
        //TODO
      }

      outTextArea.setText(result);
//    } catch (ArrayIndexOutOfBoundsException ex) {
//      outTextArea.setText("Invalid message. Enter only binary values!");
    } catch (NumberFormatException ex) {
      outTextArea.setText("Invalid sequence. Enter only numbers separated by space!");
    } catch (Exception ex) {
      ex.printStackTrace();
      outTextArea.setText(ex.toString());
    }
  }

  private ArrayList<Integer> parseList(String string) {
    ArrayList<Integer> list = new ArrayList<>();

    for (String part : string.split(" ")) {
      list.add(Integer.parseInt(part));
    }

    return list;
  }

  private void validateInput(ArrayList<Integer> w, int q) throws Exception {
    if (!isValidSequence(w)) {
      throw new Exception("Sequence " + Collections.singletonList(w) + " is not valid!");
    }

    int sum = getSum(w);
    if (sum > q) {
      throw new Exception("q must be greater or equal than " + sum + " (sum of sequence numbers)");
    }
  }

  private void selectionComboBoxActionPerformed(ActionEvent evt) {
    if (selectionComboBox.getSelectedIndex() == 0) {
      updateTextForEncryption();
    } else {
      updateTextForDecryption();
    }
  }

  private void updateTextForDecryption() {
    jButtonSubmit.setText("Decrypt message");
  }

  private void updateTextForEncryption() {
    jButtonSubmit.setText("Encrypt message");
  }

  private boolean[] getBits(String number) {
    int length = number.length();
    boolean[] result = new boolean[length];

    for (int i = 0; i < length; i++) {
      StringBuilder tmp = new StringBuilder().append(number.charAt(i));
      result[i] = toBoolean(Integer.parseInt(tmp.toString()));
    }

    return result;
  }

  private boolean isValidSequence(ArrayList<Integer> w) {
    int sum = 0, i = 0;
    boolean isValid = true;

    while (isValid && i < w.size()) {
      if (w.get(i) > sum) {
        sum += w.get(i);
        i++;
      } else {
        isValid = false;
      }
    }

    return isValid;
  }

  private boolean toBoolean(int x) {
    return x != 0;
  }

  private int getSum(ArrayList<Integer> list) {
    int sum = 0;

    for (int element : list) {
      sum += element;
    }

    return sum;
  }
}
