package frames;

import algorithms.rsa.Rsa;
import algorithms.rsa.RsaInput;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

class RsaPanel extends JPanel {
  private JTextArea outTextArea;
  private JButton jButtonSubmit;
  private JComboBox<String> jComboBox;
  private JLabel jLabel1;
  private JScrollPane jScrollPane1;
  private JSpinner messageSpinner;
  private JLabel outputLabel;
  private JLabel pLabel;
  private JSpinner pSpinner;
  private JLabel qLabel;
  private JSpinner qSpinner;
  private Rsa rsa;


  RsaPanel() {
    initComponents();
    updateLabelMessageForEncryption();
    outTextArea.setEditable(false);
  }

  private void initComponents() {
    jScrollPane1 = new JScrollPane();
    outTextArea = new JTextArea();
    outputLabel = new JLabel();
    jButtonSubmit = new JButton();
    pLabel = new JLabel();
    pSpinner = new JSpinner();
    qSpinner = new JSpinner();
    qLabel = new JLabel();
    messageSpinner = new JSpinner();
    jLabel1 = new JLabel();
    jComboBox = new JComboBox<>();
    rsa = new Rsa();


    setPreferredSize(new Dimension(600, 400));

    outTextArea.setColumns(20);
    outTextArea.setRows(5);
    jScrollPane1.setViewportView(outTextArea);

    outputLabel.setText("Output:");

    jButtonSubmit.setText("Calculate");
    jButtonSubmit.addActionListener(this::jButtonSubmitActionPerformed);

    pLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    pLabel.setText("p:");

    pSpinner.setModel(new SpinnerNumberModel(2, 2, 99999, 1));

    qSpinner.setModel(new SpinnerNumberModel(2, 2, 99999, 1));

    qLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    qLabel.setText("q:");

    jLabel1.setText("message:");

    jComboBox.setModel(new DefaultComboBoxModel<>(new String[]{"Encryption", "Decryption"}));
    jComboBox.addActionListener(this::jComboBoxActionPerformed);

    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(outputLabel, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pLabel)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pSpinner, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(qLabel)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(qSpinner, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel1)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(messageSpinner, GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE))
                            .addComponent(jComboBox, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 100, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonSubmit, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(pLabel)
                    .addComponent(pSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(qSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(qLabel)
                    .addComponent(messageSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(outputLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonSubmit)
                .addContainerGap())
    );
  }

  private void jButtonSubmitActionPerformed(ActionEvent evt) {
    try {
      String finalMessage;

      if (jComboBox.getSelectedIndex() == 0) {
        finalMessage = rsa.encrypt(new RsaInput(
            Long.parseLong(pSpinner.getValue().toString()),
            Long.parseLong(qSpinner.getValue().toString()),
            Long.parseLong(messageSpinner.getValue().toString())
        ));
      } else {
        finalMessage = rsa.encrypt(new RsaInput(
            Long.parseLong(pSpinner.getValue().toString()),
            Long.parseLong(qSpinner.getValue().toString()),
            Long.parseLong(messageSpinner.getValue().toString())
        ));
      }

      outTextArea.setText(finalMessage);
    } catch (Exception e) {
      outTextArea.setText(e.getMessage());
    }
  }

  private void jComboBoxActionPerformed(ActionEvent evt) {
    if (jComboBox.getSelectedIndex() == 0) {
      updateLabelMessageForEncryption();
    } else {
      updateLabelMessageForDecryption();
    }
  }

  private void updateLabelMessageForDecryption() {
    outputLabel.setText("Decrypted message:");
    jButtonSubmit.setText("Decrypt message");
  }

  private void updateLabelMessageForEncryption() {
    outputLabel.setText("Encrypted message:");
    jButtonSubmit.setText("Encrypt message");
  }
}
