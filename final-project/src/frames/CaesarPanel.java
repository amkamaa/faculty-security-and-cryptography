package frames;

import algorithms.caesar.CaesarCipher;

import javax.swing.*;
import java.awt.event.ActionEvent;

class CaesarPanel extends JPanel {
  private JButton jButtonSubmit;
  private JComboBox<String> jComboBoxKey;
  private JComboBox<String> jComboBoxType;
  private JLabel jLabelAlphabet;
  private JLabel jLabelInput;
  private JLabel jLabelKey;
  private JLabel jLabelOutput;
  private JScrollPane jScrollPane1;
  private JScrollPane jScrollPane2;
  private JTextArea jTextAreaInput;
  private JTextArea jTextAreaOutput;
  private JTextField jTextFieldAlphabet;
  private CaesarCipher caesarCipher;


  CaesarPanel() {
    caesarCipher = new CaesarCipher();

    initComponents();
    updateLabelMessageForEncryption();
  }

  private void initComponents() {

    jScrollPane1 = new JScrollPane();
    jTextAreaInput = new JTextArea();
    jComboBoxType = new JComboBox<>();
    jLabelInput = new JLabel();
    jLabelOutput = new JLabel();
    jScrollPane2 = new JScrollPane();
    jTextAreaOutput = new JTextArea();
    jButtonSubmit = new JButton();
    jTextFieldAlphabet = new JTextField();
    jLabelAlphabet = new JLabel();
    jComboBoxKey = new JComboBox<>();
    jLabelKey = new JLabel();

    setPreferredSize(new java.awt.Dimension(600, 400));

    jTextAreaInput.setColumns(20);
    jTextAreaInput.setRows(5);
    jScrollPane1.setViewportView(jTextAreaInput);

    jComboBoxType.setModel(new DefaultComboBoxModel<>(new String[]{"Encrypt message", "Decrypt message"}));
    jComboBoxType.addActionListener(this::jComboBoxTypeActionPerformed);

    jTextAreaOutput.setColumns(20);
    jTextAreaOutput.setRows(5);
    jTextAreaOutput.setEditable(false);
    jScrollPane2.setViewportView(jTextAreaOutput);

    jButtonSubmit.addActionListener(this::jButtonSubmitActionPerformed);

    jTextFieldAlphabet.setEditable(false);
    jTextFieldAlphabet.setText("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

    jLabelAlphabet.setText("Alphabet:");

    jComboBoxKey.setModel(new DefaultComboBoxModel<>(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26"}));

    jLabelKey.setText("Key:");

    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jComboBoxType, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                                        .addGap(101, 101, 101)
                                        .addComponent(jLabelAlphabet))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(284, 284, 284)
                                        .addComponent(jLabelKey)))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jComboBoxKey, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldAlphabet, GroupLayout.PREFERRED_SIZE, 273, GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabelInput, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelOutput, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonSubmit, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxType, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldAlphabet, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelAlphabet))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxKey, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelKey))
                        .addGap(34, 34, 34))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jLabelInput)))
                .addComponent(jScrollPane1)
                .addGap(18, 18, 18)
                .addComponent(jLabelOutput)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonSubmit)
                .addContainerGap())
    );
  }

  private void jComboBoxTypeActionPerformed(ActionEvent evt) {
    if (jComboBoxType.getSelectedIndex() == 0) {
      updateLabelMessageForEncryption();
    } else {
      updateLabelMessageForDecryption();
    }
  }

  private void jButtonSubmitActionPerformed(ActionEvent evt) {
    caesarCipher.setAlphabet(jTextFieldAlphabet.getText());
    int key = jComboBoxKey.getSelectedIndex() + 1;

    if (jComboBoxType.getSelectedIndex() == 0) {
      jTextAreaOutput.setText(caesarCipher.encrypt(jTextAreaInput.getText(), key));
    } else {
      jTextAreaOutput.setText(caesarCipher.decrypt(jTextAreaInput.getText(), key));
    }
  }

  private void updateLabelMessageForDecryption() {
    jLabelInput.setText("Encrypted message:");
    jLabelOutput.setText("Plain message:");
    jButtonSubmit.setText("Decrypt message");
  }

  private void updateLabelMessageForEncryption() {
    jLabelInput.setText("Plain message:");
    jLabelOutput.setText("Encrypted message:");
    jButtonSubmit.setText("Encrypt message");
  }
}
