package algorithms.merklehellman;

import java.util.ArrayList;
import java.util.Random;

public class MerkleHellman {
  private Random rand = new Random();

  public MerkleHellman() {
  }

  public String encrypt(boolean[] binaryValue, ArrayList<Integer> w, int q) {
    int r;

    do {
      r = rand.nextInt(q) + 1;
    } while (getGreatCommonDivisor(r, q) != 1);

    int[] pubKey = calculatePublicKey(w, q, r);
    StringBuilder result = new StringBuilder("r=" + r +
        "\nPublic Key: (" + toString(pubKey) + ")\nEncrypted message: ");

    int encrypted = 0;
    for (int i = 0; i < 8; i++) {
      if (i < pubKey.length) {
        encrypted += pubKey[i] * toInt(binaryValue[i]);
      }
    }

    return result.append(encrypted).toString();
  }

  private long getGreatCommonDivisor(long a, long b) {
    while (a != b) {
      if (a < b) {
        b -= a;
      } else {
        a -= b;
      }
    }

    return a;
  }

  private int[] calculatePublicKey(ArrayList<Integer> w, int p, int r) {
    int[] result = new int[w.size()];

    for (int i = 0; i < w.size(); i++) {
      result[i] = (r * w.get(i)) % p;
    }

    return result;
  }


  private int toInt(boolean value) {
    if (value) {
      return 1;
    } else {
      return 0;
    }
  }

  private String toString(int[] array) {
    StringBuilder result = new StringBuilder();

    for (int element : array) {
      result.append(element).append(", ");
    }

    return result.substring(0, result.length() - 2);
  }
}
