package algorithms.caesar;

public class CaesarCipher {
  private String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  public CaesarCipher() {
  }

  public void setAlphabet(String alphabet) {
    this.alphabet = alphabet;
  }

  public String encrypt(String message, int k) {
    message = message.toUpperCase();
    StringBuilder result = new StringBuilder();

    for (int i = 0; i < message.length(); i++) {
      char deltaPos = alphabet.charAt((alphabet.indexOf(message.charAt(i)) + k) % 26);
      result.append(message.charAt(i) == ' ' ? " " : deltaPos);
    }

    return result.toString();
  }

  public String decrypt(String message, int k) {
    message = message.toUpperCase();
    StringBuilder result = new StringBuilder();

    for (int i = 0; i < message.length(); i++) {
      int deltaPos = alphabet.indexOf(message.charAt(i)) - k;
      result.append(message.charAt(i) == ' ' ? " " : alphabet.charAt((deltaPos < 0 ? alphabet.length() + deltaPos : deltaPos)));
    }

    return result.toString();
  }
}