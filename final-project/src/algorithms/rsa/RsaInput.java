package algorithms.rsa;

public class RsaInput {
  long p, q, message;

  public RsaInput(long p, long q, long message) {
    this.p = p;
    this.q = q;
    this.message = message;
  }

  @Override
  public String toString() {
    return "p=" + p +
        ", q=" + q +
        ", message=" + message;
  }
}
