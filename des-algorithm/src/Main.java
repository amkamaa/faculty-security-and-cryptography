public class Main {

  public static void main(String[] args) {
    Des des = new Des();

    String message = "0123456789ABCDEF";
    String key = "AA3457799BBCDFF1";
    String received = des.encrypt(message, key);

    System.out.println("Message: \t" + message);
    System.out.println("Key: \t\t" + key);
    System.out.println("Encrypted: \t" + received);
  }
}
