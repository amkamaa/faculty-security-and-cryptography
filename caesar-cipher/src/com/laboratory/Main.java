package com.laboratory;

public class Main {

    private static String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static void main(String[] args) {
        crypt("CRIPTOGRAFIE", 7);
        decrypt("JAJSNSHWDU YTQL DXNQJ SHJNX LTQIS SXXXX", 1);
        decrypt("JAJSNSHWDU YTQL DXNQJ SHJNX LTQIS SXXXX", 2);
        decrypt("JAJSNSHWDU YTQL DXNQJ SHJNX LTQIS SXXXX", 3);
        decrypt("JAJSNSHWDU YTQL DXNQJ SHJNX LTQIS SXXXX", 4);
        decrypt("JAJSNSHWDU YTQL DXNQJ SHJNX LTQIS SXXXX", 5);
    }

    public static void crypt(String message, int k) {
        System.out.println("Encryption" + " with key " + k);
        System.out.println("Decrypted message: " + message);
        String result = "";

        for (int i = 0; i < message.length(); i++) {
            char deltaPos = alphabet.charAt((alphabet.indexOf(message.charAt(i)) + k) % 26);

            result += (message.charAt(i) == ' ' ? " " : deltaPos);
        }

        System.out.println("Encrypted message: " + result + "\n");
    }

    public static void decrypt(String message, int k) {
        System.out.println("Decryption" + " with key " + k);
        System.out.println("Encrypted message: " + message);
        String result = "";

        for (int i = 0; i < message.length(); i++) {
            int deltaPos = alphabet.indexOf(message.charAt(i)) - k;

            result += (message.charAt(i) == ' ' ? " " : alphabet.charAt((deltaPos < 0 ? alphabet.length() + deltaPos : deltaPos)));
        }
        System.out.println("Decrypted message: " + result + "\n");
    }
}

