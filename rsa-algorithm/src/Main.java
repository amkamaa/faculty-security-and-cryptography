import algorithm.Rsa;

public class Main {

    public static void main(String[] args) {
        Rsa rsa = new Rsa();

        rsa.setBitlength(1024);
        rsa.compute();

        byte[] encrypted = rsa.encrypt("Parola este in sigurnta acum!".getBytes());
        byte[] decrypted = rsa.decrypt(encrypted);

        String result = rsa.toString() + '\n';
        result += "\nPublic key: " + rsa.getPublicKey();
        result += "\nPrivate key: " + rsa.getPrivateKey() + '\n';
        result += "\nEncrypted bytes: " + rsa.bytesToString(encrypted);
        result += "\nEncrypted string: " + new String(encrypted) + '\n';
        result += "\nDecrypted bytes: " + rsa.bytesToString(decrypted);
        result += "\nDecrypted string: " + new String(decrypted);

        System.out.println(result);
    }
}
