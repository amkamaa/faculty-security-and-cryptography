package algorithm;

import java.math.BigInteger;
import java.util.Random;

public class Rsa {
    private BigInteger p, q, N, phi, e, d;
    private int bytes = 1024;
    private Random r;

    public Rsa() {
        r  = new Random();
    }

    public void setBitlength(int bitlength) {
        this.bytes = bitlength;
    }

    public String getPublicKey() {
        return "{" + e + ", " + N + "}";
    }

    public String getPrivateKey() {
        return "{" + d + ", " + p + ", " + q + "}";
    }

    public void compute() {
        p = BigInteger.probablePrime(bytes, r);
        q = BigInteger.probablePrime(bytes, r);
        N = p.multiply(q);
        phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        e = BigInteger.probablePrime(bytes / 2, r);

        while (phi.gcd(e).compareTo(BigInteger.ONE) > 0 && e.compareTo(phi) < 0)
        {
            e.add(BigInteger.ONE);
        }

        d = e.modInverse(phi);
    }

    public byte[] encrypt(byte[] message)
    {
        return new BigInteger(message).modPow(e, N).toByteArray();
    }

    public byte[] decrypt(byte[] message)
    {
        return new BigInteger(message).modPow(d, N).toByteArray();
    }

    public String bytesToString(byte[] encrypted)
    {
        String result = "";
        for (byte b : encrypted)
        {
            result += Byte.toString(b);
        }
        return result;
    }

    @Override
    public String toString() {
        return "p=" + p + '\n' +
                "q=" + q + '\n' +
                "N=" + N + '\n' +
                "phi=" + phi + '\n' +
                "e=" + e + '\n' +
                "d=" + d + '\n' +
                "bitlength=" + bytes;
    }
}
