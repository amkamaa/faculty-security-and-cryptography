package algorithm;

import java.util.List;

class HillCipher {
  static String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  void encrypt(final String inputMessage, int[][] matrix) throws Exception {
    if (matrix.length != 2 || matrix[0].length != 2) {
      throw new Exception("Invalid matrix size!");
    }

    String message = processMessage(inputMessage);
    List<Block> blocks = Block.getBlocks(message);
    StringBuilder result = new StringBuilder();
    for (Block block : blocks) {
      int[] multiply = multiplyMatrix(matrix, block.getMatrix());
      result.append(alphabet.charAt(multiply[0]));
      result.append(alphabet.charAt(multiply[1]));
    }

    System.out.println("Encryption" + " with matrix: ");
    printMatrix(matrix);
    System.out.println("Decrypted message: " + message);
    System.out.println("Encrypted message: " + result + "\n");
  }

  void decrypt(final String inputMessage, int[][] matrix) throws Exception {
    if (matrix.length != 2 || matrix[0].length != 2) {
      throw new Exception("Invalid matrix size!");
    }

    String message = removeSpaces(inputMessage);

    if (message.length() % 2 != 0) {
      throw new Exception("Message should have an even number of characters. Spaces are not counted!");
    }

    List<Block> blocks = Block.getBlocks(message);
    int[][] decryptedMatrix = decryptedMatrix(matrix);
    StringBuilder result = new StringBuilder();

    for (Block block : blocks) {
      int[] temp = multiplyMatrix(decryptedMatrix, block.getMatrix());
//      System.out.println(temp[0] + " " + temp[1]);
      result.append(alphabet.charAt(temp[0]) + "" + alphabet.charAt(temp[1]));
    }

    System.out.println("Decryption" + " with matrix: ");
    printMatrix(matrix);
    System.out.println("Encrypted message: " + message);
    System.out.println("Decrypted message: " + result + "\n");
  }

  private String removeSpaces(final String string) {
    return string.replace(" ", "").toUpperCase();
  }

  private String processMessage(final String message) {
    StringBuilder newMessage = new StringBuilder();
    newMessage.append(removeSpaces(message));

    if (newMessage.length() % 2 == 0)
      return newMessage.toString();
    return newMessage.append("Q").toString();
  }

  private int[] multiplyMatrix(int[][] matrix2x2, int[] matrix1x2) {
    return new int[]{
        (matrix2x2[0][0] * matrix1x2[0] + matrix2x2[0][1] * matrix1x2[1]) % alphabet.length(),
        (matrix2x2[1][0] * matrix1x2[0] + matrix2x2[1][1] * matrix1x2[1]) % alphabet.length()
    };
  }

  private int[][] getInverseMatrix(int[][] matrix) {
    return new int[][]{
        new int[]{matrix[1][1], -matrix[0][1]},
        new int[]{-matrix[1][0], matrix[0][0]}
    };
  }

  private int getDeterminant(int[][] matrix) {
    return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
  }

  private void printMatrix(int[][] matrix) {
    StringBuilder result = new StringBuilder();
    for (int[] line : matrix) {
      result.append('\t');
      for (int element : line) {
        result.append(element + " ");
      }
      result.append('\n');
    }

    System.out.println(result.deleteCharAt(result.length() - 1));
  }

  private int getMultiplyer(int determinant) {
    int i = 0;
    boolean found = false;

    while (i < 25 && !found) {
      if ((determinant * i) % alphabet.length() == 1) {
        found = true;
      } else {
        i++;
      }
    }

    return i;
  }

  private int[][] decryptedMatrix(int[][] matrix) {
    int[][] temp = new int[2][2];
    int[][] inverseMatrix = getInverseMatrix(matrix);

    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        temp[i][j] = inverseMatrix[i][j] < 0
            ? (alphabet.length() + inverseMatrix[i][j]) % alphabet.length()
            : inverseMatrix[i][j] % alphabet.length();
      }
    }

    return multiplyMatrix(temp, getMultiplyer(getDeterminant(matrix)));
  }

  private int[][] multiplyMatrix(final int[][] matrix, final int value) {
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        matrix[i][j] = (matrix[i][j] * value) % alphabet.length();
      }
    }

    return matrix;
  }
}
