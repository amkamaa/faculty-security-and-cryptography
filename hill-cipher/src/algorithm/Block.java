package algorithm;

import java.util.ArrayList;
import java.util.List;

import static algorithm.HillCipher.alphabet;

public class Block {
  private char c1;
  private char c2;
  private int i1;
  private int i2;

  private Block(char c1, char c2) {
    this.c1 = c1;
    this.c2 = c2;
    i1 = alphabet.indexOf(c1);
    i2 = alphabet.indexOf(c2);
  }

  public int[] getMatrix() {
    return new int[] {i1, i2};
  }

  @Override
  public String toString() {
    return c1+ "(" + i1 + ")" + c2 + "(" + i2 + ")";
  }

  static List<Block> getBlocks(final String text) {
    List<Block> blocks = new ArrayList<>();
    int index = 0;

    while (index < text.length()) {
      String di = text.substring(index, Math.min(index + 2, text.length()));
      blocks.add(new Block(di.charAt(0), di.charAt(1)));
      index = index + 2;
    }

    return blocks;
  }
}
