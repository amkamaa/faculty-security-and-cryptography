package algorithm;

public class Main {

  public static void main(String[] args) {
    HillCipher hillCipher = new HillCipher();
    try {
      hillCipher.encrypt("HATS", new int[][]{{3,3},{2,5}});
      hillCipher.decrypt("VOHY", new int[][]{{3,3},{2,5}});
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
