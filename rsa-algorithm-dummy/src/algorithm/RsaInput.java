package algorithm;

public class RsaInput {
  public long p, q, message;

  @Override
  public String toString() {
    return "p=" + p +
        ", q=" + q +
        ", message=" + message;
  }

  public RsaInput(long p, long q, long message) {
    this.p = p;
    this.q = q;
    this.message = message;
  }
}
