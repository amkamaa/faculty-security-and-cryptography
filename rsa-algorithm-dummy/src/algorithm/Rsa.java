package algorithm;

import java.math.BigInteger;

class Rsa {
  void encrypt(RsaInput input) throws Exception {
    RsaValues result = compute(input, true);

    System.out.println("Encrypted message is " + result.finalMessage);
  }

  void decrypt(RsaInput input) throws Exception {
    RsaValues result = compute(input, false);

    System.out.println("Decrypted message is " + result.finalMessage);
  }

  private RsaValues compute(RsaInput input, boolean encrypt) throws Exception {
    if (isNotPrime(input.p) || isNotPrime(input.q)) {
      throw new Exception("p or q are not prime numbers!");
    }

    long n = input.p * input.q;
    long phi = (input.p - 1) * (input.q - 1);
    long e = computeRelativePrime(phi);
    long d = computeD(e, phi);
    long decryptedText;
    if (encrypt) {
      decryptedText = fastExponent(input.message, e, n);
    } else {
      decryptedText = fastExponent(input.message, d, n);
    }

    RsaValues result = new RsaValues(n, phi, e, d, decryptedText);
    System.out.println(input);
    System.out.println(result);
    System.out.println("Public key is " + e + ", " + n);
    System.out.println("Private key is " + d);

    return result;
  }

  private boolean isNotPrime(long number) {
    return !BigInteger.valueOf(number).isProbablePrime(100);
  }

  private long computeD(final long e, final long phi) {
    boolean found = false;
    long d = 1;

    while (!found && d < phi) {
      if ((d * e) % phi == 1) {
        found = true;
      } else {
        d++;
      }
    }

    return d;
  }

  private long computeRelativePrime(long number) {
    boolean found = false;
    long e = 2;

    while (!found && e <= number) {
      if (getGreatCommonDivisor(e, number) == 1) {
        found = true;
      } else {
        e++;
      }
    }

    return e;
  }

  private long getGreatCommonDivisor(long a, long b) {
    while (a != b) {
      if (a < b) {
        b -= a;
      } else {
        a -= b;
      }
    }

    return a;
  }

  private long fastExponent(long number, long pow, long n) {
    long result = 1;
    boolean[] bits = getBinaryValue(pow);

    for (int i = bits.length - 1; i >= 0; i--) {
      result = (result * result) % n;

      if (bits[i]) {
        result = (result * number) % n;
      }
    }

    return result;
  }

  private boolean[] getBinaryValue(long number) {
    int length = Long.toString(number, 2).length();
    boolean[] bits = new boolean[length];

    for (int i = 0; i < length; i++) {
      bits[i] = (number & (1 << i)) != 0;
    }

    return bits;
  }
}
