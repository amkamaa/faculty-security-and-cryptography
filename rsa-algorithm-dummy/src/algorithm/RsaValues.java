package algorithm;

class RsaValues {
  long n, phi, e, d, finalMessage;

  RsaValues(long n, long phi, long e, long d, long finalMessage) {
    this.n = n;
    this.phi = phi;
    this.e = e;
    this.d = d;
    this.finalMessage = finalMessage;
  }

  @Override
  public String toString() {
    return "n=" + n +
        ", phi=" + phi +
        ", e=" + e +
        ", d=" + d;
  }
}
