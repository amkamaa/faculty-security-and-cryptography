package algorithm;

public class Main {

  public static void main(String[] args) {
    Rsa rsa = new Rsa();

    testEncrypt(rsa, new RsaInput(7, 17,19));
    testDecrypt(rsa, new RsaInput(7, 17, 66));

    System.out.println("------------------------");

    testEncrypt(rsa, new RsaInput(101, 113,632));
    testDecrypt(rsa, new RsaInput(101, 113, 3234));

    System.out.println("------------------------");

    testEncrypt(rsa, new RsaInput(7, 11,14));
    testDecrypt(rsa, new RsaInput(7, 11, 42));
  }

  private static void testEncrypt(Rsa rsa, RsaInput input) {
    try {
      rsa.encrypt(input);
    } catch (Exception e) {
      e.printStackTrace();
    }
    System.out.println();
  }

  private static void testDecrypt(Rsa rsa, RsaInput input) {
    try {
      rsa.decrypt(input);
    } catch (Exception e) {
      e.printStackTrace();
    }
    System.out.println();
  }
}
